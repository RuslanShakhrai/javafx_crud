module CrudSystem {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires javafx.base;
    requires java.sql;
    requires javafx.graphics;

    opens CrudSystem.Controller to javafx.fxml, java.sql,javafx.base;
    opens CrudSystem.Model to javafx.base;
    opens CrudSystem to javafx.graphics;
}