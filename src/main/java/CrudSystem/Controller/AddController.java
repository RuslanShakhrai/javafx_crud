package CrudSystem.Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class AddController extends MainController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button BackButton;

    @FXML
    private TextField TitleField;

    @FXML
    private TextField AuthorField;

    @FXML
    private TextField YearField;

    @FXML
    private TextField PagesField;

    @FXML
    private Button SubmitButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    public void handleExitButtonAction(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Back to the MainForm");
        alert.setHeaderText("Confirm Action");
        alert.setContentText("Do you really want to go back?");
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get().equals(ButtonType.OK)) {
            switchScene(BackButton, "/view/Main.fxml");
        }
    }

    @FXML
    public void handleSubmitButtonAction(ActionEvent actionEvent) {
        String title = TitleField.getText();
        String author = AuthorField.getText();
        String year = YearField.getText();
        String pages = PagesField.getText();
        if (title.equals("") || author.equals("") || year.equals("") | pages.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Incorrect input data");
            alert.setContentText("All input fields must be filled in!");
            alert.showAndWait();
        }
        try {
            PreparedStatement preparedStatement = AddController.connection.prepareStatement("INSERT INTO books(`title`,`author`,`year`,`pages`) VALUES (?,?,?,?)");
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, author);
            preparedStatement.setInt(3, Integer.parseInt(year));
            preparedStatement.setInt(4, Integer.parseInt(pages));
            preparedStatement.executeUpdate();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Query execution result");
            alert.setContentText("The data that you entered was successfully added");
            FlushData();
            alert.showAndWait();
            switchScene(SubmitButton, "/view/Main.fxml");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        catch (NumberFormatException exception){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Invalid number format");
            alert.setContentText("Numbers are in the wrong format");
            alert.showAndWait();
        }
    }

    protected void FlushData() {
        TitleField.setText("");
        AuthorField.setText("");
        YearField.setText("");
        PagesField.setText("");
    }

}
