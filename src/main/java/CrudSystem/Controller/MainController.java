package CrudSystem.Controller;

import CrudSystem.Model.Book;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private static final String URL = "jdbc:mysql://localhost:3306/library";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    protected static Connection connection;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException throwables) {
            System.err.println("Error: " + throwables.getMessage());
            throwables.printStackTrace();
        }

    }

    private static int from = 0;
    private static int to = 0;
    private static int itemPerPage = 20;
    @FXML
    private Label IdLabel;

    @FXML
    private Label TitleLabel;

    @FXML
    private Label AuthorLabel;

    @FXML
    private Label YearLabel;

    @FXML
    private Label PagesLabel;
    @FXML
    private TableView<Book> tvBooks;

    @FXML
    private TableColumn<Book, Integer> idColumn;

    @FXML
    private TableColumn<Book, String> TitleColumn;

    @FXML
    private TableColumn<Book, String> AuthorColumn;

    @FXML
    private TableColumn<Book, Integer> YearColumn;

    @FXML
    private TableColumn<Book, Integer> PagesColumn;

    @FXML
    private TextField IdField;

    @FXML
    private TextField TitleField;

    @FXML
    private TextField AuthorField;

    @FXML
    private TextField YearField;

    @FXML
    private TextField PagesField;

    @FXML
    private Button InsertButton;

    @FXML
    private Button UpdateButton;

    @FXML
    private Button DeleteButton;

    @FXML
    private Button ExitButton;

    @FXML
    private Button AboutButton;

    @FXML
    private Pagination Pagination;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        showPagination();
    }

    private Node createPage(int pageIndex) {
        from = pageIndex * itemPerPage;
        to = itemPerPage;
        tvBooks.setItems(FXCollections.observableList(getBooksList()));
        return tvBooks;
    }

    private int countOfRows() {
        int count = 0;
        String query = "SELECT count(*) from books";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultSet = preparedStatement.executeQuery(query);
            resultSet.first();
            count = resultSet.getInt(1);
            System.out.println(count);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return count;
    }

    private void showPagination() {
        int pageCount = (countOfRows() / itemPerPage) + 1;
        setColumns();
        Pagination.setPageCount(pageCount);
        Pagination.setPageFactory(this::createPage);
    }

    @FXML
    private void handleButtonAction(ActionEvent actionEvent) {
        if (actionEvent.getSource() == InsertButton) {
            switchScene(InsertButton, "/view/Add.fxml");
        } else if (actionEvent.getSource() == DeleteButton) {
            deleteRecord();
        } else {
            updateRecord();
        }
    }

    @FXML
    private void ExitConfirm(ActionEvent event) {
        Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION);
        exitAlert.setTitle("Exit");
        exitAlert.setHeaderText("Confirm action");
        exitAlert.setContentText("Do you really want to get out?");
        Optional<ButtonType> option = exitAlert.showAndWait();
        if (option.get().equals(ButtonType.OK)) {
            System.exit(0);
        }
    }

    @FXML
    private void AboutProgram(ActionEvent event) {
        Alert aboutAlert = new Alert(Alert.AlertType.INFORMATION);
        aboutAlert.setTitle("About");
        aboutAlert.setHeaderText("Information about program");
        aboutAlert.setContentText("This program is developed for educational purposes in order to study CRUD operations");
        aboutAlert.showAndWait();
    }

    @FXML
    private void handleMouseAction(MouseEvent event) {
        try {
            Book book = tvBooks.getSelectionModel().getSelectedItem();
            IdField.setText(String.valueOf(book.getId()));
            TitleField.setText(book.getTitle());
            AuthorField.setText(book.getAuthor());
            YearField.setText(String.valueOf(book.getYear()));
            PagesField.setText(String.valueOf(book.getPages()));
            showInputsLabels();
        } catch (NullPointerException exception) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Incorrect action");
            alert.setContentText("You didn't select any row from table");
            alert.showAndWait();
        }
    }

    private ObservableList<Book> getBooksList() {
        ObservableList<Book> booksList = FXCollections.observableArrayList();
        String query = "SELECT * FROM books limit " + from + "," + to;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Book book = new Book(resultSet.getInt("id"),
                        resultSet.getString("title"),
                        resultSet.getString("author"),
                        resultSet.getInt("year"),
                        resultSet.getInt("pages"));
                booksList.add(book);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return booksList;
    }

    private void setColumns() {
        idColumn.setCellValueFactory(new PropertyValueFactory<Book, Integer>("id"));
        TitleColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));
        AuthorColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("author"));
        YearColumn.setCellValueFactory(new PropertyValueFactory<Book, Integer>("year"));
        PagesColumn.setCellValueFactory(new PropertyValueFactory<Book, Integer>("pages"));
    }

    private void updateRecord() {
        String id = IdField.getText();
        String title = TitleField.getText();
        String author = AuthorField.getText();
        String year = YearField.getText();
        String pages = PagesField.getText();
        if (id.equals("") || title.equals("") || author.equals("")
                || year.equals("") || pages.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Incorrect input data");
            alert.setContentText("You didn't fill all inputs");
            alert.showAndWait();
        }
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE books SET title = ?, author = ?, year = ?, pages = ? WHERE id = ?");
            preparedStatement.setString(1, title.trim());
            preparedStatement.setString(2, author.trim());
            preparedStatement.setInt(3, Integer.parseInt(year));
            preparedStatement.setInt(4, Integer.parseInt(pages));
            preparedStatement.setInt(5, Integer.parseInt(id));
            preparedStatement.executeUpdate();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Query execution result");
            alert.setContentText("The record that you selected was successfully updated");
            alert.showAndWait();
            showPagination();
        }
        catch (SQLException throwables){
            throwables.printStackTrace();
        }
        catch (NumberFormatException exception){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Invalid number format");
            alert.setContentText("Numbers are in the wrong format");
            alert.showAndWait();
        }
    }

    private void deleteRecord() {
        Book book = tvBooks.getSelectionModel().getSelectedItem();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM books WHERE id = ?");
            preparedStatement.setInt(1, book.getId());
            preparedStatement.executeUpdate();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setHeaderText("Query execution result");
            alert.setContentText("The record that you selected was successfully deleted");
            alert.showAndWait();
            FlushData();
            hideInputsLabels();
            showPagination();
        }
        catch (SQLException throwables){
            throwables.printStackTrace();
        }
    }

    private void showInputsLabels() {
        IdLabel.setVisible(true);
        TitleLabel.setVisible(true);
        AuthorLabel.setVisible(true);
        YearLabel.setVisible(true);
        PagesLabel.setVisible(true);
        IdField.setVisible(true);
        TitleField.setVisible(true);
        AuthorField.setVisible(true);
        YearField.setVisible(true);
        PagesField.setVisible(true);
    }

    private void hideInputsLabels() {
        IdLabel.setVisible(false);
        TitleLabel.setVisible(false);
        AuthorLabel.setVisible(false);
        YearLabel.setVisible(false);
        PagesLabel.setVisible(false);
        IdField.setVisible(false);
        TitleField.setVisible(false);
        AuthorField.setVisible(false);
        YearField.setVisible(false);
        PagesField.setVisible(false);
    }

    protected void switchScene(Button button, String path) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource(path));
            Stage stage = (Stage) ((button).getScene().getWindow());
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }

    protected void FlushData() {
        IdField.setText("");
        TitleField.setText("");
        AuthorField.setText("");
        YearField.setText("");
        PagesField.setText("");
    }
}
